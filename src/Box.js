export const Box = (props) => {
  return (
    <div
      className="box-component"
      style={{ backgroundColor: props.box.color }}
      draggable={props.draggable}
      onDragStart={props.onDragStart({ id: props.box.id })}
      onDragOver={props.onDragOver({ id: props.box.id })}
      onDrop={props.onDrop({ id: props.box.id })}
    ></div>
  );
};
