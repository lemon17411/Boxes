import React, { useEffect, useState } from "react";
import { Box } from "./Box";

import "./App.css";

function App() {
  const [boxes, setBox] = useState([]);

  useEffect(() => {
    let defaultBox = [];
    for (let i = 0; i < 64; i++) {
      defaultBox.push({ id: i, color: `rgb(0, 0, 0, ${(i+10)/100})` });
    }
    setBox(defaultBox);
  }, []);

  const swapBoxes = (fromBox, toBox) => {
    let newBoxes = boxes.slice();
    let fromIndex = -1;
    let toIndex = -1;

    for (let i = 0; i < newBoxes.length; i++) {
      if (newBoxes[i].id === fromBox.id) {
        fromIndex = i;
      }
      if (newBoxes[i].id === toBox.id) {
        toIndex = i;
      }
    }

    if (fromIndex !== -1 && toIndex !== -1) {
      let { fromId, ...fromRest } = newBoxes[fromIndex];
      let { toId, ...toRest } = newBoxes[toIndex];
      newBoxes[fromIndex] = { id: fromBox.id, ...toRest };
      newBoxes[toIndex] = { id: toBox.id, ...fromRest };

      setBox(newBoxes);
    }
  };

  const handleDragStart = (data) => (event) => {
    let fromBox = JSON.stringify({ id: data.id });
    event.dataTransfer.setData("dragContent", fromBox);
  };

  const handleDragOver = () => (event) => {
    event.preventDefault();
    return false;
  };

  const handleDrop = (data) => (event) => {
    event.preventDefault();

    let fromBox = JSON.parse(event.dataTransfer.getData("dragContent"));
    let toBox = { id: data.id };

    swapBoxes(fromBox, toBox);
    return false;
  };

  return (
    <div className="box">
      {boxes.map((box) => (
        <Box
          box={box}
          key={box.id}
          draggable="true"
          onDragStart={handleDragStart}
          onDragOver={handleDragOver}
          onDrop={handleDrop}
        />
      ))}
    </div>
  );
}

export default App;
